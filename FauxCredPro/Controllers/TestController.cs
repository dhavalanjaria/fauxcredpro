﻿using FauxCredPro.FauxBackend;
using FauxCredPro.FauxBackend.CustomerManagement;
using FauxCredPro.FauxBackend.Facilities;
using FauxCredPro.FineractAdapter;
using FauxCredPro.FineractAdapter.CustomerManagement;
using FauxCredPro.FineractAdapter.FacilitiesAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FauxCredPro.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RunCustomerTest()
        {
            ViewBag.Data = "OK";

            // Important notes:
            // First, the Dictionary keys need to be exactly the right case and the right
            // spelling, otherwise Fineract crashes.
            // Secondly, dateFormat and locale are mandatory fields for basically every call it seems
            // They are added to the Dictionary in their respective adapter functions for now.

            // First we create a client.

            ICustomer customer = new CustomerAdapter();

            Dictionary<string, object> testClientData = new Dictionary<string, object>();
            testClientData["fullname"] = "DemoUser" + new Random().Next(1000);
            testClientData["officeId"] = 1;
            testClientData["active"] = true;
            testClientData["activationDate"] = "1 July 2020";

            int newClientId = newClientId = customer.AddCustomer(testClientData);

            if (newClientId > 0)
            {
                ViewBag.ClientId = newClientId;

                // If customer is created
                Dictionary<string, object> testLoanData = new Dictionary<string, object>();

                // Important note: If the some of the fields are invalid
                // Fineract crashes with an error saying that the "given id is invalid". 
                // As of this writing it isn't known which field is causing the issue,
                // but the Fineract crash is confirmed.

                testLoanData["clientId"] = newClientId;
                testLoanData["productId"] = 1;
                testLoanData["principal"] = 10000;
                testLoanData["loanTermFrequency"] = 12;
                testLoanData["loanTermFrequencyType"] = 2;
                testLoanData["loanType"] = "individual";
                testLoanData["numberOfRepayments"] = 12;
                testLoanData["repaymentEvery"] = 1;
                testLoanData["repaymentFrequencyType"] = 2;
                testLoanData["interestRatePerPeriod"] = 10;
                testLoanData["amortizationType"] = 1;
                testLoanData["interestType"] = 0;
                testLoanData["interestCalculationPeriodType"] = 1;
                testLoanData["transactionProcessingStrategyId"] = 1;
                testLoanData["expectedDisbursementDate"] = "2 July 2020";
                testLoanData["submittedOnDate"] = "2 July 2020";

                ICustomerLoans loans = new CustomerLoansAdapter();
                int newLoanId = loans.SubmitNewLoanApplication(testLoanData);

                if (newLoanId > 0)
                {
                    ViewBag.NewLoanId = newLoanId;

                    Dictionary<string, object> approvalData = new Dictionary<string, object>();
                    approvalData["approvedOnDate"] = "2 July 2020";
                    string status = loans.ApproveLoan(approvalData, newLoanId);

                    if (status == "Approved")
                    {
                        Dictionary<string, object> disbursalData = new Dictionary<string, object>();
                        disbursalData["transactionAmount"] = 1000;
                        disbursalData["fixedEmiAmount"] = 100;
                        disbursalData["actualDisbursementDate"] = "2 July 2020";
                        disbursalData["paymentTypeId"] = "1";
                        disbursalData["note"] = "";
                        disbursalData["accountNumber"] = "accno123";
                        disbursalData["checkNumber"] = "chec123";
                        disbursalData["routingCode"] = "rou123";
                        disbursalData["receiptNumber"] = "rec123";
                        disbursalData["bankNumber"] = "ban123";

                        // Locale and date format are added in the Disburse function.
                        IDisbursal disbursal = new DisbursalAdapter();
                        var res = disbursal.DisburseLoan(disbursalData, newLoanId);

                    }
                }
            }

            // Submit a loan application for said client
            // Then approve that loan.

            return View("Index", ViewBag);
        }
    }
}