﻿using FauxCredPro.FauxBackend.CustomerManagement;
using FineractProxy.Endpoints.Loans;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FauxCredPro.FineractAdapter.CustomerManagement
{
    public class CustomerLoansAdapter : ICustomerLoans
    {
        LoansEndpoints endpoint = new LoansEndpoints(AdapterConfig.BaseAddress, AdapterConfig.Base64StringForBasicAuth);

        public int SubmitNewLoanApplication(Dictionary<string, object> loanData)
        {
            loanData["locale"] = "en";
            loanData["dateFormat"] = "dd MMMM yyyy";

            Dictionary<string, object> res = new Dictionary<string, object>();
            Debug.WriteLine("Submitting New Loan Application...");
            Task.Run(() => { res = endpoint.SubmitNewLoanApplicationAsync(loanData).Result; }).Wait();

            object retval = 0;
            res.TryGetValue("resourceId", out retval);
            Debug.WriteLine("Done... resourceId = " + retval);
            return Convert.ToInt32(retval);               
        }

        public string ApproveLoan(Dictionary<string, object> approvalData, int loanId)
        {
            approvalData["locale"] = "en";
            approvalData["dateFormat"] = "dd MMMM yyyy";

            Dictionary<string, object> res = new Dictionary<string, object>();
            Debug.WriteLine("Approving loan... " + loanId);
            Task.Run(() => { res = endpoint.ApproveLoansAsync(approvalData, loanId).Result; }).Wait();
            Debug.WriteLine("Done.");

            object resourceId;
            res.TryGetValue("resourceId", out resourceId);

            if (resourceId != null)
                return "Approved";
            else
                return "Approval failed";
            
        }
    }
}