﻿using FauxCredPro.FauxBackend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineractProxy.Endpoints.Client;
using System.Configuration;
using System.Text;
using FauxCredPro.FauxBackend.CustomerManagement;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FauxCredPro.FineractAdapter
{
    /// <summary>
    /// Class basically overrides methods from CustomerBackend but forwards the requests to FineractProxy
    /// </summary>
    public class CustomerAdapter : ICustomer
    {

        public ClientsEndpoints endpoint = new ClientsEndpoints(AdapterConfig.BaseAddress, AdapterConfig.Base64StringForBasicAuth);
        
        /// <summary>
        /// Add or Update Customer. Note: Here we're assuming that there is an interfact that 
        /// CustomerAdapter conforms to, which there should be.
        /// </summary>
        /// <param name="clientDataDict">Dictionary<string, object> containg client data.</string></param>
        /// <returns></returns>
        public int AddCustomer(Dictionary<string, object> clientDataDict)
        {
            // YOUR CODE HERE

            // NOTE: DEMO CODE

            clientDataDict["dateFormat"] = "dd MMMM yyyy";
            clientDataDict["locale"] = "en";

            Dictionary<string, object> res = new Dictionary<string, object>();
            Debug.WriteLine("Creating new client...");
            Task.Run(() => { res = endpoint.PostClientsAsync(clientDataDict).Result; }).Wait();
            Debug.WriteLine("Done... clientId = " + res["clientId"]);
            return Convert.ToInt32(res["clientId"]); // If updated, false if added.
        }

        public new List<string> GetAllClients()
        {
            // YOUR CODE HERE

            // The following is example code.
            Dictionary<string, object> res = new Dictionary<string, object>();
            Task.Run(() => { res = endpoint.GetAsync(String.Empty).Result; }).Wait();

            List<String> retval = new List<string>();

            foreach (var clientId in res)
            {
                retval.Append(res["clientId"]);
            }

            return retval;
        }
    }
}