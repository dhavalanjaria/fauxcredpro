﻿using FauxCredPro.FauxBackend.Facilities;
using FineractProxy.Endpoints.Org;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FauxCredPro.FineractAdapter.FacilitiesAdapters
{
    public class RetailLoansAdapter : IRetailLoans
    {
        public LoanProductsEndpoints endpoint = new LoanProductsEndpoints(
            FineractAdapter.AdapterConfig.BaseAddress,
            FineractAdapter.AdapterConfig.Base64StringForBasicAuth);


        public new int CreatePersonalLoan(Dictionary<string, object> customerData)
        {
            // YOUR CODE HERE

            // The following is example code.
            // Retail loan defautls
            customerData["currencyCode"] = "INR";
            customerData["digitsAfterDecimal"] = 2;
            customerData["amortizationType"] = 1;
            customerData["accountingRule"] = 1;
            customerData["dateFormat"] = "dd MMMM yyyy";
            customerData["locale"] = "en";
            customerData["daysInYearType"] = 1;
            customerData["daysInMonthType"] = 1;
            customerData["fundSourceAccountId"] = 1;
            customerData["loanPortfolioAccountId"] = 1;
            customerData["interestOnLoanAccountId"] = 1;
            customerData["incomeFromFeeAccountId"] = 1;
            customerData["incomeFromPenaltyAccountId"] = 1;
            customerData["writeOffAccountId"] = 1;
            customerData["transfersInSuspenseAccountId"] = 1;
            customerData["overpaymentLiabilityAccountId"] = 1;
            customerData["transactionProcessingStrategyId"] = 4; // RBI rules for payment

            Dictionary<string, object> res = new Dictionary<string, object>();
            Task.Run(() => { res = endpoint.CreateLoanProductAsync(customerData).Result; }).Wait();

            return Convert.ToInt32(res["resourceId"]);
        }
    }
}