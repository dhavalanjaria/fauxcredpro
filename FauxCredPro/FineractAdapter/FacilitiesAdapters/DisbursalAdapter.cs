﻿using FauxCredPro.FauxBackend.Facilities;
using FineractProxy.Endpoints.Loans;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FauxCredPro.FineractAdapter.FacilitiesAdapters
{
    public class DisbursalAdapter : IDisbursal
    {
        public LoansEndpoints endpoints = new LoansEndpoints(AdapterConfig.BaseAddress, AdapterConfig.Base64StringForBasicAuth);

        public string DisburseLoan(Dictionary<string, object> transaction, int loanId)
        {
            transaction["locale"] = "en";
            transaction["dateFormat"] = "dd MMMM yyyy";
            Debug.WriteLine("Disbursing loan... " + loanId);
            Dictionary<string, object> res = new Dictionary<string, object>();
            Task.Run(() => { res = endpoints.DisburseLoans(transaction, loanId).Result; }).Wait();
            Debug.WriteLine("Done. resourceId = " + res["resourceId"]);
            return Convert.ToString(res["resourceId"]);
        }
    }
}