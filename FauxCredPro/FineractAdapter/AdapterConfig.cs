﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace FauxCredPro.FineractAdapter
{
    /// <summary>
    /// Configuration values for all the adapters. This contains values such as baseaddress and base64 string used for Basic Auth
    /// </summary>
    public static class AdapterConfig
    {
        public static string BaseAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["baseAddress"];
            }
        }
        public static string Base64StringForBasicAuth
        {
            get
            {
                var username = ConfigurationManager.AppSettings["username"];
                var password = ConfigurationManager.AppSettings["password"];
                var byteArr = Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password);
                return Convert.ToBase64String(byteArr);
            }
        }
    }
}