﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineractProxy.Endpoints.Org;
using FauxCredPro.FauxBackend.Accounts;
using System.Threading.Tasks;

namespace FauxCredPro.FineractAdapter.AccountsAdapters
{
    public class SavingsAccountAdapter : ISavingsAccounts
    {
        public SavingsProductEndpoints endpoint = new SavingsProductEndpoints(AdapterConfig.BaseAddress, AdapterConfig.Base64StringForBasicAuth);

        public bool UpdateSavingsAccount(Dictionary<string, object> changes, int accountId)
        {
            // DEMO CODE
            Dictionary<string, object> res = new Dictionary<string, object>();

            Task.Run(() => { res = endpoint.UpdateSavingsAccountAsync(changes, accountId).Result; }).Wait();

            if (res["changes"] != null)
                return true;
            return false;
        }
    }
}