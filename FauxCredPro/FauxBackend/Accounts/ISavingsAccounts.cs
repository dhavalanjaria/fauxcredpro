﻿using System.Collections.Generic;

namespace FauxCredPro.FauxBackend.Accounts
{
    public interface ISavingsAccounts
    {
        bool UpdateSavingsAccount(Dictionary<string, object> changes, int accountId);
    }
}