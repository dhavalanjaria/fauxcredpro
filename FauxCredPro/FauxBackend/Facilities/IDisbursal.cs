﻿using System.Collections.Generic;

namespace FauxCredPro.FauxBackend.Facilities
{
    public interface IDisbursal
    {
        string DisburseLoan(Dictionary<string, object> transaction, int loanId);
    }
}