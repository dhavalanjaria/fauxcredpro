﻿using System.Collections.Generic;

namespace FauxCredPro.FauxBackend.Facilities
{
    public interface IRetailLoans
    {
        int CreatePersonalLoan(Dictionary<string, object> customerData);
    }
}