﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FauxCredPro.FauxBackend.CustomerManagement
{
    public interface ICustomer
    {
        int AddCustomer(Dictionary<string, object> clientDataDict);
        List<string> GetAllClients();
    }
}