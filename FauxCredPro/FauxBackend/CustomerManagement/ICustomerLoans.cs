﻿using System.Collections.Generic;

namespace FauxCredPro.FauxBackend.CustomerManagement
{
    public interface ICustomerLoans
    {
        string ApproveLoan(Dictionary<string, object> approvalData, int loanId);
        int SubmitNewLoanApplication(Dictionary<string, object> loanData);
    }
}