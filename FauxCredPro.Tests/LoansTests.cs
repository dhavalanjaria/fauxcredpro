﻿using System;
using System.Collections.Generic;
using System.Configuration;
using FauxCredPro.FauxBackend.CustomerManagement;
using FauxCredPro.FauxBackend.Facilities;
using FauxCredPro.FineractAdapter.CustomerManagement;
using FauxCredPro.FineractAdapter.FacilitiesAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FauxCredPro.Tests
{
    [TestClass]
    public class LoansTests
    {
        [TestInitialize]
        public void SetUp()
        {
            ConfigurationManager.AppSettings["baseAddress"] = "https://localhost:8443/fineract-provider/api/v1/";
            ConfigurationManager.AppSettings["username"] = "mifos";
            ConfigurationManager.AppSettings["password"] = "password";
        }

        [TestMethod]
        public void TestNewLoanApplication()
        {
            
            Dictionary<string, object> testLoanData = new Dictionary<string, object>();

            testLoanData["clientId"] = 10;
            testLoanData["productId"] = 1;
            testLoanData["principal"] = 10000;
            testLoanData["loanTermFrequency"] = 12;
            testLoanData["loanTermFrequencyType"] = 2;
            testLoanData["loanType"] = "individual";
            testLoanData["numberOfRepayments"] = 12;
            testLoanData["repaymentEvery"] = 1;
            testLoanData["repaymentFrequencyType"] = 2;
            testLoanData["interestRatePerPeriod"] = 10;
            testLoanData["amortizationType"] = 1;
            testLoanData["interestType"] = 0;
            testLoanData["interestCalculationPeriodType"] = 1;
            testLoanData["transactionProcessingStrategyId"] = 1;
            testLoanData["expectedDisbursementDate"] = "2 July 2020";
            testLoanData["submittedOnDate"] = "2 July 2020";
            testLoanData["locale"] = "en";
            testLoanData["dateFormat"] = "dd MMMM yyyy";

            ICustomerLoans loans = new CustomerLoansAdapter();
            int newLoanId = loans.SubmitNewLoanApplication(testLoanData);

            Assert.IsTrue(newLoanId > 0);


        }

        [TestMethod]
        public void TestDisbursal()
        {
            Dictionary<string, object> disbursalData = new Dictionary<string, object>();
            disbursalData["transactionAmount"] = 1000;
            disbursalData["fixedEmiAmount"] = 100;
            disbursalData["actualDisbursementDate"] = "30 June 2020";
            disbursalData["paymentTypeId"] = "1";
            disbursalData["note"] = "";
            disbursalData["accountNumber"] = "accno123";
            disbursalData["checkNumber"] = "chec123";
            disbursalData["routingCode"] = "rou123";
            disbursalData["receiptNumber"] = "rec123";
            disbursalData["bankNumber"] = "ban123";

            DisbursalAdapter disbursalObj = new DisbursalAdapter();
            string res = disbursalObj.DisburseLoan(disbursalData, 2);

            Assert.IsTrue(res.Length > 0);
        }
    }
}
