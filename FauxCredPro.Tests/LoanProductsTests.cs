﻿using System;
using FineractProxy.Endpoints.Org;
using FauxCredPro.FauxBackend.Facilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FauxCredPro.FineractAdapter.FacilitiesAdapters;
using System.Collections.Generic;
using System.Configuration;
using FauxCredPro.FineractAdapter.AccountsAdapters;

namespace FauxCredPro.Tests
{
    [TestClass]
    public class LoanProductsTests
    {
        [TestInitialize]
        public void SetUp()
        {
            ConfigurationManager.AppSettings["baseAddress"] = "https://localhost:8443/fineract-provider/api/v1/";
            ConfigurationManager.AppSettings["username"] = "mifos";
            ConfigurationManager.AppSettings["password"] = "password";
        }

        [TestMethod]
        public void TestNewPersonalLoanFails()
        {
            RetailLoansAdapter loanCreator = new RetailLoansAdapter();
            Dictionary<string, object> facility = new Dictionary<string, object>();

            facility["name"] = "XyzPersonalLoan";
            facility["shortName"] = "XYZ";
            facility["shortName"] = "XYZ";
            facility["numberOfRepayments"] = "7";
            facility["repaymentEvery"] = "7";

            Assert.ThrowsException<AggregateException>(() =>
            {
                var res = loanCreator.CreatePersonalLoan(facility);
            });

        }

        [TestMethod]
        public void TestNewPersonalLoan()
        {
            RetailLoansAdapter loanCreator = new RetailLoansAdapter();
            Dictionary<string, object> facility = new Dictionary<string, object>();

            facility["name"] = "XyzPersonalLoan3";
            facility["shortName"] = "XYZ3";
            facility["numberOfRepayments"] = "7";
            facility["repaymentEvery"] = "7";
            facility["repaymentFrequencyType"] = 0;
            facility["interestType"] = 0;
            facility["isInterestRecalculationEnabled"] = false;
            facility["interestRatePerPeriod"] = 12;
            facility["interestRateFrequencyType"] = 2;
            facility["interestCalculationPeriodType"] = 1;
            
            int res = loanCreator.CreatePersonalLoan(facility);

            Assert.IsTrue(res > 0);

        }

        [TestMethod]
        public void TestOverdraftLimitChange()
        {
            Dictionary<string, object> changes = new Dictionary<string, object>();
            changes["overdraftLimit"] = 1200;
            changes["locale"] = "en";

            SavingsAccountAdapter savingsAccount = new SavingsAccountAdapter();
            bool res = savingsAccount.UpdateSavingsAccount(changes, 1);

            Assert.IsTrue(res);

        }

    }
}
