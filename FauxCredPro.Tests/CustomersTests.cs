﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using FauxCredPro.FauxBackend;
using FauxCredPro.FauxBackend.CustomerManagement;
using FauxCredPro.FineractAdapter;
using FineractProxy.ConnectionManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FauxCredPro.Tests
{
    [TestClass]
    public class CustomersTests
    {
        [TestInitialize]
        public void SetUp()
        {
            ConfigurationManager.AppSettings["baseAddress"] = "https://localhost:8443/fineract-provider/api/v1/";
            ConfigurationManager.AppSettings["username"] = "mifos";
            ConfigurationManager.AppSettings["password"] = "password";
        }

        [TestMethod]
        public void TestGetClients()
        {
            ICustomer customerBackend = new CustomerAdapter();
            var res = customerBackend.GetAllClients();

            Assert.IsTrue(res.Count > 0);

        }

        [TestMethod]
        public void TestPostClientsFail()
        {
            // This can either be the Fineract Backend OR the Flexcube backend.
            ICustomer customer = new CustomerAdapter();

            Dictionary<string, object> postData = new Dictionary<string, object>();
            postData["fullname"] = "DemoUser";
            postData["officeId"] = 1;
            postData["active"] = true;
            postData["activationDate"] = "21 June 2020";

            var ex = Assert.ThrowsException<AggregateException>(
                () => { var res = customer.AddCustomer(postData);  });

            Assert.IsTrue(ex.InnerException is Fineract400Exception);
        }

        [TestMethod]
        public void TestPostClients()
        {
            ICustomer customer = new CustomerAdapter();

            Dictionary<string, object> postData = new Dictionary<string, object>();
            postData["fullname"] = "DemoUserX" + new Random().Next(20303);
            postData["officeId"] = 1;
            postData["active"] = true;
            postData["activationDate"] = "1 July 2020";
            postData["dateFormat"] = "dd MMMM yyyy";
            postData["locale"] = "en";

            var res = customer.AddCustomer(postData);

            Assert.IsTrue(res > 0);
        }
    }
}
